import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';

class Attempt {
  final List<int> colors;
  late final int correct;
  late final int exists;
  Attempt(this.colors, List<int> solution) {
    // sorry, no sane way to return multiple values...
    var result = _MyHomePageState._getCorrectAndExists(colors, solution);
    correct= result[0];
    exists = result[1];
  }
}

final _randomGenerator = Random();

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {

    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);

    return MaterialApp(
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: Colors.deepPurple,
      ),
      home: const MyHomePage(title: 'Color Code'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  static const _initialColors = [
    '0xfffe8a71', '0xff2ab7ca', '0xff7bc043',
    '0xff4a4e4d', '0xfff6cd61', '0xff7a1f62',
  ];

  final Future<SharedPreferences> _prefs = SharedPreferences.getInstance();

  late List<Color> _codeColors;

  static const _colorsNumber = 6;
  static const _solutionLength = 4;

  var _attempt = List<int?>.filled(_solutionLength, null);
  late List<int> _solution;

  List<Attempt> _attempts = [];
  bool _isPickingColor = false;
  int? _currentGuessPosition;
  bool _solved = false;
  bool _isLoading = true;

  @override
  void initState() {
    super.initState();
    _loadPrefs();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: _isLoading ?  const Center(child: CircularProgressIndicator()) : SafeArea(
        // minimum: const EdgeInsets.all(16.0),
        child:Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
          // available colors
            Row(
              children: [
                for (int i = 0; i < 6; i++)
                  Expanded(
                    flex: 1,
                    child: InkWell(
                      onTap: () => _setRandomColor(i),
                      child: Container(
                        color: _codeColors[i],
                        child: SizedBox(
                          height: MediaQuery.of(context).size.height * 0.08
                        ),
                      ),
                    )
                  )
              ]
            ),
            const SizedBox(height: 10,),
            // all the previous guesses
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                for (final attempt in _attempts)
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      for (int i = 0; i < attempt.colors.length; i++)
                        Expanded(
                          flex: 2,
                          child: InkWell(
                            onTap: () => _setGuessColor(i, attempt.colors[i]),
                            child:
                              Container(
                                color: _codeColors[attempt.colors[i]],
                                child: SizedBox(
                                  height: MediaQuery.of(context).size.height * 0.08
                                ),
                              ),
                          )
                        ),
                      Expanded(
                        flex: 1,
                        child: GridView.count(
                          shrinkWrap: true,
                          crossAxisCount: 2,
                          // mainAxisSpacing: 0.0,
                          // crossAxisSpacing: 0.0,
                          // padding: const EdgeInsets.all(5),
                          children: [
                            for (int i = 0; i < attempt.correct; i++)
                              FractionallySizedBox(
                                widthFactor: 0.4,
                                heightFactor: 0.4,
                                child: Container(
                                  decoration: BoxDecoration(
                                    color: Colors.black,
                                    border: Border.all(color: Colors.black),
                                    shape: BoxShape.circle,
                                  ),
                                ),
                              ),
                            for (int i = 0; i < attempt.exists; i++)
                              FractionallySizedBox(
                                widthFactor: 0.4,
                                heightFactor: 0.4,
                                child: Container(
                                  decoration: BoxDecoration(
                                    border: Border.all(color: Colors.black),
                                    shape: BoxShape.circle,
                                  ),
                                ),
                              ),
                          ]
                        )
                      ),
                    ]
                  ),
              ]
            ),
            const SizedBox(height: 10,),
            Expanded(
              child: Align(
                alignment: Alignment.bottomCenter,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    // set the colors in the guess line
                    if (_isPickingColor)
                    Row(
                      children: [
                        for (int i = 0; i < 6; i++)
                          Expanded(
                            flex: 1,
                            child: InkWell(
                              onTap: () => _pickColor(i),
                              child: Container(
                                color: _codeColors[i],
                                child: SizedBox(
                                  height: MediaQuery.of(context).size.height * 0.08
                                ),
                              ),
                            )
                          )
                      ]
                    ),
                    if (_isPickingColor)
                      const SizedBox(height: 10,),
                    // the guess line
                    Row(
                      children: _solved ?
                        [
                          Expanded(
                              flex: 2 * _solutionLength,
                              child: Container(),
                          ),
                          Expanded(
                            flex: 1,
                            child: ElevatedButton(
                              onPressed: _reset,
                              style: ElevatedButton.styleFrom(
                                shape: const CircleBorder(),
                                padding: const EdgeInsets.all(15),
                                backgroundColor: const Color(0xff4a4e4d),
                              ),
                              child: const Icon(Icons.replay_outlined)
                            )
                          )
                        ] : [
                          for (int i = 0; i < _solutionLength; i++)
                            Expanded(
                              flex: 2,
                              child: InkWell(
                                onTap: () => _startPickingColor(i),
                                onLongPress: () => _resetColor(i),
                                child: Container(
                                  // TODO: should the border be lighter / thinner
                                  decoration:
                                    BoxDecoration(
                                      border: Border.all(color: _attempt[i] == null || _currentGuessPosition == i ? Colors.black : _codeColors[_attempt[i]!]),
                                      color: _attempt[i] == null ? const Color.fromARGB(0, 0, 0, 0) : _codeColors[_attempt[i]!],
                                    ),
                                  child: SizedBox(
                                    height: MediaQuery.of(context).size.height * 0.08
                                  ),
                                ),
                              )
                          ),
                          Expanded(
                            flex: 1,
                            child: ElevatedButton(
                              onPressed: _attempt.any((c) => c == null) ? null : _checkGuess,
                              style: ElevatedButton.styleFrom(
                                shape: const CircleBorder(),
                                padding: const EdgeInsets.all(15),
                                backgroundColor: const Color(0xff4a4e4d),
                              ),
                              child: const Icon(Icons.check_sharp),
                            )
                          ),
                      ]
                    ),
                  ]
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
  static List<int> _getNewSolution() => List<int>.generate(_solutionLength, (_) => _randomGenerator.nextInt(_colorsNumber));

  static Color _getRandomColor() {
    return Color.fromARGB(255, _randomGenerator.nextInt(256), _randomGenerator.nextInt(256), _randomGenerator.nextInt(256));
  }

  void _loadPrefs() async {
    final prefs = await _prefs;
    var solution = prefs.getString('solution');
    if (solution == null) {
      solution = _getNewSolution().join(',');
      prefs.setString('solution', solution);
    }

    setState(() {
      // retrieve from ['0x000000', '0x000000', ...] (or its dec representation)
      _codeColors = (prefs.getStringList('colors') ?? _initialColors).map((color) => Color(int.parse(color))).toList();
      // retreive from '1,2,3,4]'
      _solution = solution!.split(',').map((c) => int.parse(c)).toList();
      _attempts = (prefs.getStringList('attempts') ?? <String>[])
        .map((a) => Attempt(a.split(',').map((c) => int.parse(c)).toList(), _solution)).toList();
      // for debugging purposes, show the solution as the first attempt
      _attempts.add(Attempt(_solution, _solution));
      _isLoading = false;
    });
  }

  void _setRandomColor(int i) {
    setState(() =>
      _codeColors[i] = _getRandomColor());

    (() async {
      final prefs = await _prefs;
      prefs.setStringList('colors', _codeColors.map((c) => c.value.toString()).toList());
    })();
  }

  void _setGuessColor(int i, int color) {
    setState(() =>
      _attempt[i] = color);
  }

  void _pickColor(int i) {
    setState(() {
      _isPickingColor = false;
      _attempt[_currentGuessPosition!] = i;
      _currentGuessPosition = null;
    });
  }

  void _startPickingColor(int i) {
    setState(() {
      if (_isPickingColor && _currentGuessPosition == i) {
        _currentGuessPosition = null;
        _isPickingColor = false;
      } else {
        _currentGuessPosition = i;
        _isPickingColor = true;
      }
    });
  }

  void _resetColor(int i) {
    setState(() {
      _attempt[i] = null;
    });
  }

  void _checkGuess() {
    setState(() {
      _attempts.add(Attempt(_attempt.map((v) => v!).toList(), _solution));
      _attempt = List<int?>.filled(_solutionLength, null);
      _solved = _attempts.last.correct == _solutionLength;
    });

    (() async {
      final prefs = await _prefs;
      prefs.setStringList('attempts', _attempts.map((a) => a.colors.join(',')).toList());
    })();
  }

  void _reset()  {
    setState(() {
      _solution = _getNewSolution();
      _attempts = [];
      _solved = false;
    });

    (() async {
      final prefs = await _prefs;
      prefs.setString('solution', _solution.join(','));
    })();
  }

  static List<int> _getCorrectAndExists(List<int?> attempt, List<int> solution) {
    int correct = 0;
    int exists = 0;

    List<int> incorrectAttempt = [];
    List<int> incorrectSolution = [];

    for (int i = 0; i < _solutionLength; i += 1) {
      if (attempt[i] == solution[i]) {
        correct += 1;
      } else {
        incorrectAttempt.add(attempt[i]!);
        incorrectSolution.add(solution[i]);
      }
    }
    var attemptCounter = incorrectAttempt.fold<Map<int, int>>({}, (map, element) {
      map[element] = (map[element] ?? 0) + 1;
      return map;
    });
    var solutionCunter = incorrectSolution.fold<Map<int, int>>({}, (map, element) {
      map[element] = (map[element] ?? 0) + 1;
      return map;
    });

    attemptCounter.forEach((key, value) {
      if (solutionCunter.containsKey(key)) {
        exists += min(value, solutionCunter[key]!);
      }
    });
    return [correct, exists];
  }
}
